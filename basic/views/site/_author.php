<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>
<div class="post">
    <h4><?= Html::encode($model->name) ?></h2>
    <ol>
    <?php foreach ($model->getBooks()->all() as $book): ?>
        <li><?= HtmlPurifier::process($book->title) ?></li>  
    <?php endforeach; ?>
    </ol>
   

</div>
