<?php

use yii\widgets\ListView;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="body-content">
        <?php
        echo ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_author',
            'viewParams' => [
                'fullView' => true,
                'context' => 'main-page',
            ],
        ]);
        ?>
    </div>
</div>
