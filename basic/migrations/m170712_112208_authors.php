<?php

use yii\db\Migration;

class m170712_112208_authors extends Migration {
    /* public function safeUp()
      {

      }

      public function safeDown()
      {
      echo "m170712_112208_authors cannot be reverted.\n";

      return false;
      } */

    // Use up()/down() to run migration code without a transaction.
    public function up() {
        $this->createTable('authors', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->defaultValue('512')->null(),
        ]);

        return true;
    }

    public function down() {
        $this->dropTable('authors');

        return true;
    }

}
