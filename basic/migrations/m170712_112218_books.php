<?php

use yii\db\Migration;

class m170712_112218_books extends Migration
{
 /*   public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m170712_112218_books cannot be reverted.\n";

        return false;
    }*/

    
    // Use up()/down() to run migration code without a transaction.
    public function up() {
        $this->createTable('books', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->defaultValue('512')->null(),
            'authors_id' => $this->integer()->notNull(),
        ]);
        
        $this->createIndex(
            'fk_books_authors_idx',
            'books',
            'authors_id'
        );
        
        $this->addForeignKey(
            'fk_books_authors',
            'books',
            'authors_id',
            'authors',
            'id',
            'NO ACTION'
        );

        return true;
    }

    public function down() {
        
        $this->dropForeignKey(
            'fk_books_authors',
            'books'
        );

        $this->dropIndex(
            'fk_books_authors_idx',
            'books'
        );
        
        $this->dropTable('books');
        
        return true;
    }
    
}
