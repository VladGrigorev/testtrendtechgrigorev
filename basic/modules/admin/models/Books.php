<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "books".
 *
 * @property string $title
 * @property integer $authors
 */
class Books extends \yii\db\ActiveRecord
{
    
    public function fields() {
        return [
            // имя поля "title"
            'title',
            // имя поля "authors", значение определяется callback-ом PHP
            'authors' => function () {
                return $this->author->name;
            },
        ];
    }
   
    public static function tableName()
    {
        return 'books';
    }

    public function rules()
    {
        return [
            [['authors_id','title'], 'required'],
            [['authors_id'], 'integer'],
            [['title'], 'string', 'max' => 512],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Title',
            'authors_id' => 'Authors ID',
        ];
    }
    
    public function getAuthor()
    {
        return $this->hasOne(Authors::className(), ['id' => 'authors_id']);
    }
}
