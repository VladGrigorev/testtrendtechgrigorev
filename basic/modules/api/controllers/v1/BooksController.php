<?php

namespace app\modules\api\controllers\v1;

use yii\rest\ActiveController;
use app\modules\admin\models\Books;
use yii\base\Exception;
use yii\web\BadRequestHttpException;
use Yii;

/**
 * Default controller for the `api` module
 */
class BooksController extends ActiveController {

    
    
    public $modelClass = 'app\modules\admin\models\Books';

    public function init()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    }

    protected function verbs() {
        return [
            'update' => ['PUT', 'PATCH', 'POST'],
        ];
    }

    public function actions() {
        $actions = parent::actions();
        unset($actions['update']);
        return $actions;
    }

    public function actionList() {
        try {
            return Books::find()->all();
        } catch (BadRequestHttpException $e) {
            return $e;
        }
    }

    public function actionById($id) {
        try {
            if (!$book = Books::findOne($id)) {
                throw new BadRequestHttpException('Exception. Not found book with this id.');
            }
            return $book;
        } catch (BadRequestHttpException $e) {
            return $e;
        }
    }

    public function actionUpdate($id) {
        try {
            if (!$book = Books::findOne($id)) {
                throw new adRequestHttpException('Exception. Not found book with this id.');
            }
            if (!$book->title = Yii::$app->request->post('title')) {
                throw new BadRequestHttpException('Exception. Field title is requried.');
            }
            if (!$book->authors_id = Yii::$app->request->post('authors_id')) {
                throw new BadRequestHttpException('Exception. Field authors_id is requried.');
            }

            if ($book->save()) {
                return $book;
            } else {
                throw new BadRequestHttpException('Exception. Database error.');
            }
        } catch (BadRequestHttpException $e) {
            return $e;
        }
    }

}
